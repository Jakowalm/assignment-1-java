package com.rpg;

import com.rpg.character.Hero;
import com.rpg.character.attributes.PrimaryAttribute;
import com.rpg.classes.subclasses.Mage;
import com.rpg.classes.subclasses.Ranger;
import com.rpg.classes.subclasses.Rogue;
import com.rpg.classes.subclasses.Warrior;
import com.rpg.item.Armor;
import com.rpg.item.Item;
import com.rpg.item.Weapon;
import com.rpg.item.exceptions.InvalidArmorException;
import com.rpg.item.exceptions.InvalidWeaponException;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    static ArrayList<Item> items = new ArrayList<>();
    static Hero hero;

    /**
     * Creates a basic hero before opening up a menus with options for how to interact with the program.
     *
     * @param args command line arguments. Should be empty
     * @throws InvalidWeaponException Can be thrown by Hero.getDPS(), but won't when the program runs.
     * @throws InvalidArmorException Can be thrown by Hero.getDPS(), but won't when the program runs.
     */
    public static void main(String[] args) throws InvalidWeaponException, InvalidArmorException {
        Scanner scanner = new Scanner(System.in);
        hero = createCharacter(scanner);
        System.out.println(hero);
        boolean finished = false;
        while (!finished) {
            System.out.println("What would you like to do?");
            System.out.println("""
                    Create an item: [c]
                    Equip an item: [e]
                    Level up: [l]
                    Deal damage: [d]
                    Quit: [q]""");
            String input = scanner.nextLine();
            switch (input.toLowerCase()) {
                case "c" -> createItem(scanner);
                case "e" -> equipItem(scanner);
                case "l" -> hero.levelUp();
                case "d" -> System.out.println("Damage: " + hero.getDPS());
                case "q" -> finished = true;
            }
        }
    }

    /**
     * Basis for the creation of items. Really just goes through all the relevant questions for generating a new item.
     *
     * @param scanner Scanner for system input
     */
    static void createItem(Scanner scanner) {
        System.out.println("What type of item do you want to create? [weapon|armor]");
        String type = scanner.nextLine().toLowerCase();
        while (!type.equals("weapon") && !type.equals("armor")) {
            type = scanner.nextLine().toLowerCase();
        }
        Item item;
        if (type.equals("weapon")) {
            item = new Weapon();
            createBaseInfo(scanner,item,type);
            createWeapon(scanner,item);
        }
        else {
            item = new Armor();
            createBaseInfo(scanner,item,type);
            createArmor(scanner,item);
        }
        items.add(item);
    }

    /**
     * Sets the name and level for weapons. In its own method since this should be done for all items regardless of type.
     *
     * @param scanner Scanner for system input
     * @param item The item that's being created
     * @param type The type of the item. Really just here to be printed to the console.
     */
    static void createBaseInfo(Scanner scanner, Item item,String type) {
        System.out.println("What is the name of your " + type + "?");
        item.setName(scanner.nextLine());
        System.out.println("What is the level of " + item.getName() + "?");
        item.setRequiredLevel(getInt(scanner));
    }

    /**
     * Sets the item slot to WEAPON before setting what weapon type is being created.
     * Then sets the damage and attack speed for the weapon.
     *
     * @param scanner Scanner for system input
     * @param item The item that's being created
     */
    static void createWeapon(Scanner scanner, Item item) {
        item.setItemSlot(Item.slot.WEAPON);
        System.out.println("What type of weapon is it? [axe|bow|dagger|hammer|staff|sword|wand]");
        String type = scanner.nextLine().toUpperCase();
        boolean typeSelected = false;
        while (!typeSelected) {
            switch (type) {
                case "AXE" -> {
                    ((Weapon) item).setType(Item.types.AXE);
                    typeSelected = true;
                }
                case "BOW" -> {
                    ((Weapon) item).setType(Item.types.BOW);
                    typeSelected = true;
                }
                case "DAGGER" -> {
                    ((Weapon) item).setType(Item.types.DAGGER);
                    typeSelected = true;
                }
                case "HAMMER" -> {
                    ((Weapon) item).setType(Item.types.HAMMER);
                    typeSelected = true;
                }
                case "STAFF" -> {
                    ((Weapon) item).setType(Item.types.STAFF);
                    typeSelected = true;
                }
                case "SWORD" -> {
                    ((Weapon) item).setType(Item.types.SWORD);
                    typeSelected = true;
                }
                case "WAND" -> {
                    ((Weapon) item).setType(Item.types.WAND);
                    typeSelected = true;
                }
                default -> type = scanner.nextLine().toUpperCase();
            }
        }


        System.out.println("How much damage does it deal?");
        ((Weapon) item).setDamage(getInt(scanner));
        System.out.println("What is its attack speed?");
        while (true) {
            String speed = scanner.nextLine();
            try {
                double num = Double.parseDouble(speed);
                ((Weapon) item).setAttackSpeed(num);
                break;
            }
            catch(NumberFormatException e) {
                System.out.println("Please enter a valid double number");
            }
        }
    }

    /**
     * Sets the item slot for the armor as well as the type of armor.
     * Then it sets the stats for the piece.
     *
     * @param scanner Scanner for system input
     * @param item The item that's being created
     */
    static void createArmor(Scanner scanner, Item item) {
        System.out.println("What slot does your armor go in? [head|body|legs]");
        String slot = scanner.nextLine().toLowerCase();
        while (!slot.equals("head") && !slot.equals("body") && !slot.equals("legs")) {
            slot = scanner.nextLine().toLowerCase();
        }
        switch (slot) {
            case "head" -> item.setItemSlot(Item.slot.HEAD);
            case "body" -> item.setItemSlot(Item.slot.BODY);
            case "legs" -> item.setItemSlot(Item.slot.LEGS);
        }

        System.out.println("What type of armor is " + item.getName() + "? [cloth|leather|mail|plate]");
        String type = scanner.nextLine().toLowerCase();
        while (!type.equals("cloth") && !type.equals("leather") && !type.equals("mail") && !type.equals("plate")) {
            type = scanner.nextLine().toLowerCase();
        }
        Item.types t;
        switch (type) {
            case "cloth" -> t = Item.types.CLOTH;
            case "leather" -> t = Item.types.LEATHER;
            case "mail" -> t = Item.types.MAIL;
            default -> t = Item.types.PLATE;
        }
        ((Armor) item).setType(t);

        System.out.println("How much strength does " + item.getName() + " add?");
        int str = getInt(scanner);
        System.out.println("How much dexterity does " + item.getName() + " add?");
        int dex = getInt(scanner);
        System.out.println("How much intelligence does " + item.getName() + " add?");
        int intl = getInt(scanner);
        System.out.println("How much vitality does " + item.getName() + " add?");
        int vit = getInt(scanner);
        ((Armor) item).setStats(new PrimaryAttribute(str,dex,intl,vit));
    }

    /**
     * Prints an item list and then attempts to equip it to the hero.
     *
     * @param scanner Scanner for system input
     */
    static void equipItem(Scanner scanner) {
        System.out.println("What item do you want to equip?");
        printItems();
        boolean selectedItem = false;
        Item item = null;
        while (!selectedItem) {
            String name = scanner.nextLine().toLowerCase();
            for (Item i : items) {
                if (i.getName().toLowerCase().equals(name)) {
                    selectedItem = true;
                    item = i;
                }
            }
        }
        try {
            hero.giveItem(item);
        }
        catch (InvalidWeaponException | InvalidArmorException err) {
            System.out.println(err);
        }
    }

    /**
     * Just prints the weapon names
     */
    static void printItems() {
        for (Item i : items) {
            System.out.println(i.getName());
        }
    }

    /**
     * Helper method to get an int from the command line without the program crashing on faulty input.
     *
     * @param scanner Scanner for system input
     * @return returns an int from the command line
     */
    static int getInt(Scanner scanner) {
        while (true) {
            String damage = scanner.nextLine();
            try {
                return Integer.parseInt(damage);
            }
            catch(NumberFormatException e) {
                System.out.println("Please enter a valid integer number");
            }
        }
    }

    /**
     * Method to create the basic hero in the main method.
     *
     * @param scanner Scanner for system input
     * @return returns a basic hero with name and class
     */
    static Hero createCharacter(Scanner scanner) {
        System.out.println("What's your hero's name?");
        Hero hero = new Hero(scanner.nextLine());
        System.out.println("What class is your hero? [mage|ranger|rogue|warrior]");
        String characterClass = scanner.nextLine().toLowerCase();
        boolean characterSuccess = true;
        do {
            switch (characterClass) {
                case "mage" -> hero.giveClass(new Mage());
                case "ranger" -> hero.giveClass(new Ranger());
                case "rogue" -> hero.giveClass(new Rogue());
                case "warrior" -> hero.giveClass(new Warrior());
                default -> characterSuccess = false;
            }
        } while (!characterSuccess);
        return hero;
    }
}
