package com.rpg.character;

import com.rpg.character.attributes.PrimaryAttribute;
import com.rpg.classes.BaseClass;
import com.rpg.item.Item;
import com.rpg.item.exceptions.InvalidArmorException;
import com.rpg.item.exceptions.InvalidWeaponException;

import java.util.HashMap;

public class Hero {
    private final String name;
    private int level = 1;
    private BaseClass characterClass;
    private PrimaryAttribute baseAttrs;
    private PrimaryAttribute totalAttrs;
    private final HashMap<Item.slot, Item> items = new HashMap<>();

    /**
     *
     * @param name the name of the hero
     */
    public Hero(String name) {
        this.name = name;
    }

    /**
     * Levels up the character using the levelUp() method in its class.
     * In order to ensure this doesn't crash, it checks that the character has a class first.
     * It also increases the character level.
     */
    public void levelUp() {
        if (characterClass!= null) characterClass.levelUp(baseAttrs);
        level++;
    }

    /**
     * Updates the total attributes before calculating the dps of the Hero.
     * The dps is calculated from weapon dps and the sum of attributes with other equipment.
     *
     * @return returns the dps of the Hero.
     * @throws InvalidArmorException As a safety measure for item types, they throw the opposite type's error if it is attempted to use them in the wrong slot.
     * @throws InvalidWeaponException As a safety measure for item types, they throw the opposite type's error if it is attempted to use them in the wrong slot.
     */
    public double getDPS() throws InvalidArmorException, InvalidWeaponException {
        updateTotalAttrs();
        double weaponMod = 1;
        for (Item.slot key: items.keySet()) {
            if (key == Item.slot.WEAPON) {
                weaponMod = items.get(key).getDPS();
            }
        }
        return weaponMod*(1+(characterClass.mainAttribute(totalAttrs)/100.0));
    }

    /**
     * Assigns a new class to the Hero.
     * Because a character cannot multiclass, it resets all the stats for the character to level one.
     * This could (and maybe should) be implemented in a way where it reset everything and checked if the items were still valid too
     * , but this can also be controlled in the code.
     *
     * @param newClass the new class for the character
     */
    public void giveClass(BaseClass newClass) {
        characterClass = newClass;
        baseAttrs = newClass.levelOne();
    }

    /**
     * Checks if an item can be added to the character and then adds it.
     * If the item can't be added, nothing will be returned as an error is thrown in the item's class.
     *
     * @param newItem the item that should be added
     * @return returns a boolean that is true if the item could be equipped.
     * @throws InvalidWeaponException thrown if the item is a weapon that can't be equipped.
     * @throws InvalidArmorException thrown if the item is an armor piece that can't be equipped.
     */
    public boolean giveItem(Item newItem) throws InvalidWeaponException, InvalidArmorException {
        boolean success = newItem.tryEquip(this);
        if (success) {
            items.put(newItem.getItemSlot(),newItem);
            if (newItem.getItemSlot() != Item.slot.WEAPON) updateTotalAttrs();
        }
        return success;
    }

    /**
     * Resets the totalAttributes and then calculates them based on the base attributes and armor.
     *
     * @throws InvalidWeaponException As a safety measure for item types, they throw the opposite type's error if it is attempted to use them in the wrong slot.
     */
    private void updateTotalAttrs() throws InvalidWeaponException {
        totalAttrs = new PrimaryAttribute(0,0,0,0);
        totalAttrs.sumAttributes(baseAttrs);
        for (Item.slot s : items.keySet()) {
            if (s != Item.slot.WEAPON) {
                totalAttrs.sumAttributes(items.get(s).getArmorStats());
            }
        }
    }

    /**
     * A toString method. Not much explaining needed.
     *
     * @return returns a string with some information about the Hero.
     */
    @Override
    public String toString() {
        try {
            updateTotalAttrs();
        } catch (InvalidWeaponException ignored) {}
        return "Hero{" +
                "name='" + name + '\'' +
                ", Level=" + level +
                ", Class=" + characterClass +
                ", Strength=" + totalAttrs.getStrength() +
                ", Dexterity=" + totalAttrs.getDexterity() +
                ", Intelligence=" + totalAttrs.getIntelligence() +
                ", Vitality=" + totalAttrs.getVitality() +
                '}';
    }

    //Setters and getters. Not much explanation needed.

    public PrimaryAttribute getBaseAttrs() {
        return baseAttrs;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public BaseClass getCharacterClass() {
        return characterClass;
    }
}
