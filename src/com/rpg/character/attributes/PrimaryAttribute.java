package com.rpg.character.attributes;

import java.util.Objects;

public class PrimaryAttribute {
    private int strength;
    private int dexterity;
    private int intelligence;
    private int vitality;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrimaryAttribute that = (PrimaryAttribute) o;
        return getStrength() == that.getStrength() && getDexterity() == that.getDexterity() && getIntelligence() == that.getIntelligence() && getVitality() == that.getVitality();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getStrength(), getDexterity(), getIntelligence(), getVitality());
    }

    public PrimaryAttribute(int strength, int dexterity, int intelligence, int vitality) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
        this.vitality = vitality;
    }

    public void sumAttributes(PrimaryAttribute newAttrs) {
        strength += newAttrs.getStrength();
        dexterity += newAttrs.getDexterity();
        intelligence += newAttrs.getIntelligence();
        vitality += newAttrs.getVitality();
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getVitality() {
        return vitality;
    }

    public void setVitality(int vitality) {
        this.vitality = vitality;
    }
}
