package com.rpg.classes;

import com.rpg.character.attributes.PrimaryAttribute;

public abstract class BaseClass {

    /**
     * Method for levelling up a hero. Each subclass increases different stats on level up.
     *
     * @param attr the attributes of the hero that should be levelled up.
     */
    abstract public void levelUp(PrimaryAttribute attr);

    /**
     * Each subclass uses different stats for their main attribute, so each subclass will return their main attribute here.
     *
     * @param attr the attribute set of the hero.
     * @return returns the number of the primary stat for the character.
     */
    abstract public int mainAttribute(PrimaryAttribute attr);

    /**
     * Method for giving attributes to a level one hero. Again, each class has different stats.
     *
     * @return the level one attributes of the class
     */
    abstract public PrimaryAttribute levelOne();
}
