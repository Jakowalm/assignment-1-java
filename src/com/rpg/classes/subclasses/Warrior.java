package com.rpg.classes.subclasses;

import com.rpg.character.attributes.PrimaryAttribute;
import com.rpg.classes.BaseClass;

public class Warrior extends BaseClass {

    @Override
    public PrimaryAttribute levelOne() {
        return new PrimaryAttribute(5,2,1,10);
    }

    public void levelUp(PrimaryAttribute attr) {
        attr.setStrength(attr.getStrength()+3);
        attr.setDexterity(attr.getDexterity()+2);
        attr.setIntelligence(attr.getIntelligence()+1);
        attr.setVitality(attr.getVitality()+5);
    }
    public int mainAttribute(PrimaryAttribute attr) {
        return attr.getStrength();
    }

    @Override
    public String toString() {
        return "Warrior";
    }
}
