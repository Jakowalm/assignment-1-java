package com.rpg.classes.subclasses;

import com.rpg.character.attributes.PrimaryAttribute;
import com.rpg.classes.BaseClass;

public class Mage extends BaseClass {

    @Override
    public PrimaryAttribute levelOne() {
        return new PrimaryAttribute(1,1,8,5);
    }

    @Override
    public void levelUp(PrimaryAttribute attr) {
        attr.setStrength(attr.getStrength()+1);
        attr.setDexterity(attr.getDexterity()+1);
        attr.setIntelligence(attr.getIntelligence()+5);
        attr.setVitality(attr.getVitality()+3);
    }

    public int mainAttribute(PrimaryAttribute attr) {
        return attr.getIntelligence();
    }

    @Override
    public String toString() {
        return "Mage";
    }
}
