package com.rpg.classes.subclasses;

import com.rpg.character.attributes.PrimaryAttribute;
import com.rpg.classes.BaseClass;

public class Ranger extends BaseClass {

    @Override
    public PrimaryAttribute levelOne() {
        return new PrimaryAttribute(1,7,1,8);
    }

    @Override
    public void levelUp(PrimaryAttribute attr) {
        attr.setStrength(attr.getStrength()+1);
        attr.setDexterity(attr.getDexterity()+5);
        attr.setIntelligence(attr.getIntelligence()+1);
        attr.setVitality(attr.getVitality()+2);
    }

    public int mainAttribute(PrimaryAttribute attr) {
        return attr.getDexterity();
    }

    @Override
    public String toString() {
        return "Ranger";
    }
}
