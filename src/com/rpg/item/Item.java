package com.rpg.item;

import com.rpg.character.Hero;
import com.rpg.character.attributes.PrimaryAttribute;
import com.rpg.item.exceptions.InvalidArmorException;
import com.rpg.item.exceptions.InvalidWeaponException;

abstract public class Item {
    protected String name;
    protected int requiredLevel;
    protected slot itemSlot;

    //The different slots equipment can be placed in.
    public enum slot {
        WEAPON,
        HEAD,
        BODY,
        LEGS
    }

    //The different types of items that exist.
    public enum types {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE,
        AXE,
        BOW,
        DAGGER,
        HAMMER,
        STAFF,
        SWORD,
        WAND
    }

    /**
     * Only armor items overwrite this method
     * , so if for some reason someone tries to get stats for a weapon, it will throw an error.
     *
     * @return returns the stats for a piece of armor
     * @throws InvalidWeaponException thrown if the method is called on a weapon.
     */
    public PrimaryAttribute getArmorStats() throws InvalidWeaponException {
        throw new InvalidWeaponException("Can't get stats for a weapon!");
    }


    /**
     * Only weapon items overwrite this method
     * , so if for some reason someone tries to get stats for a piece of armor, it will throw an error.
     *
     * @return returns the dps for a weapon
     * @throws InvalidArmorException thrown if the method is called on a piece of armor.
     */
    public double getDPS() throws InvalidArmorException {
        throw new InvalidArmorException("Can't get DPS from an armor piece!");
    }

    /**
     * Will check whether the item can be equipped to hte hero in question.
     *
     * @param hero the hero that should equip the item
     * @return returns true or false depending on whether it was successful.
     * @throws InvalidWeaponException thrown if a weapon can't be equipped.
     * @throws InvalidArmorException thrown if a piece of armor can't be equipped.
     */
    abstract public boolean tryEquip(Hero hero) throws InvalidWeaponException, InvalidArmorException;

    //Basic setters and getters. That's it.

    public void setName(String name) {
        this.name = name;
    }

    public void setRequiredLevel(int requiredLevel) {
        this.requiredLevel = requiredLevel;
    }

    public slot getItemSlot() {
        return itemSlot;
    }

    public void setItemSlot(slot itemSlot) {
        this.itemSlot = itemSlot;
    }

    public String getName() {
        return name;
    }
}
