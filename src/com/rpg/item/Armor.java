package com.rpg.item;

import com.rpg.character.Hero;
import com.rpg.character.attributes.PrimaryAttribute;
import com.rpg.item.exceptions.InvalidArmorException;

import java.util.Arrays;
import java.util.HashMap;

public class Armor extends Item {

    types type;
    PrimaryAttribute stats;

    //Map with information about what classes are allowed to use what armor types.
    HashMap<String, types[]> allowedCombinations = new HashMap<>();
    types[] mageArmors = {types.CLOTH};
    types[] rangerArmors = {types.LEATHER,types.MAIL};
    types[] rogueArmors = {types.LEATHER,types.MAIL};
    types[] warriorArmors = {types.MAIL,types.PLATE};

    public Armor() {
        allowedCombinations.put("Mage", mageArmors);
        allowedCombinations.put("Ranger", rangerArmors);
        allowedCombinations.put("Rogue", rogueArmors);
        allowedCombinations.put("Warrior", warriorArmors);
    }

    @Override
    public boolean tryEquip(Hero hero) throws InvalidArmorException {
        if (!Arrays.asList(allowedCombinations.get(hero.getCharacterClass().toString())).contains(type)) {
            throw new InvalidArmorException("This armor type can not be equipped by " + hero.getName() + "!");
        }
        else if (hero.getLevel() < requiredLevel) {
            throw new InvalidArmorException("This armor is too high level for " + hero.getName() + "!");
        }
        return true;
    }

    //Getters and setters.

    public void setType(types type) {
        this.type = type;
    }

    public void setStats(PrimaryAttribute stats) {
        this.stats = stats;
    }

    @Override
    public PrimaryAttribute getArmorStats() {
        return stats;
    }
}
