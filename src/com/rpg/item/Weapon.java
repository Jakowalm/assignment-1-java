package com.rpg.item;

import com.rpg.character.Hero;
import com.rpg.item.exceptions.InvalidWeaponException;
import java.util.Arrays;
import java.util.HashMap;

public class Weapon extends Item {

    types type;
    private int damage;
    private double attackSpeed;

    //Map with information about what classes are allowed to use what weapon types.
    HashMap<String, types[]> allowedCombinations = new HashMap<>();
    types[] mageWeapons = {types.STAFF, types.WAND};
    types[] rangerWeapons = {types.BOW};
    types[] rogueWeapons = {types.DAGGER, types.SWORD};
    types[] warriorWeapons = {types.AXE, types.HAMMER, types.SWORD};

    public Weapon() {
        allowedCombinations.put("Mage",mageWeapons);
        allowedCombinations.put("Ranger",rangerWeapons);
        allowedCombinations.put("Rogue",rogueWeapons);
        allowedCombinations.put("Warrior",warriorWeapons);
    }

    @Override
    public boolean tryEquip(Hero hero) throws InvalidWeaponException {
        if (!Arrays.asList(allowedCombinations.get(hero.getCharacterClass().toString())).contains(type)) {
            throw new InvalidWeaponException("This weapon type can not be equipped by " + hero.getName() + "!");
        }
        else if (hero.getLevel() < requiredLevel) {
            throw new InvalidWeaponException("This weapon is too high level for " + hero.getName() + "!");
        }
        return true;
    }

    //Getters and setters
    @Override
    public double getDPS()  {
        return Math.round((damage*attackSpeed)*10000.0)/10000.0;
    }

    public void setType(types type) {
        this.type = type;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }
}
