# RPG character creator


Welcome to the RPG character creator.
In it, you can make the RPG character of your dreams!
Give them names, classes, weapons, armor and levels!

To run the program, simply run it and enter the info as it is being requested.
None of the entries are case-sensitive, so just type hoWEvEr yoU liKe.
If you're curious as to how it all works, just take a peek in the source code!
Everything is well commented and explained.

If you want to check out the program with only the required features and no command line functionality, you can go to the branch `no-extra-features`.
