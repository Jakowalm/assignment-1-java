package com.rpg.classes.subclasses;

import com.rpg.character.Hero;
import com.rpg.character.attributes.PrimaryAttribute;
import com.rpg.item.exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {
    @Test
    void rogueLevelOne_testingLevelOneAttributes_expecting_2_6_1_8() {
        Hero testHero = new Hero("John");
        testHero.giveClass(new Rogue());
        PrimaryAttribute testAttrs = new PrimaryAttribute(2,6,1,8);
        assert testHero.getBaseAttrs().equals(testAttrs);
    }

    @Test
    void rogueLevelUp_testingLevelUpAttributes_expecting_3_10_2_11() {
        Hero testHero = new Hero("John");
        testHero.giveClass(new Rogue());
        testHero.levelUp();
        PrimaryAttribute testAttrs = new PrimaryAttribute(3,10,2,11);
        assert testHero.getBaseAttrs().equals(testAttrs);
    }
}