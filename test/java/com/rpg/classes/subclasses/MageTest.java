package com.rpg.classes.subclasses;

import com.rpg.character.Hero;
import com.rpg.character.attributes.PrimaryAttribute;
import com.rpg.item.exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {

    @Test
    void levelOne_testingLevelOneAttributes_expecting_1_1_8_5() {
        Hero testHero = new Hero("John");
        testHero.giveClass(new Mage());
        PrimaryAttribute testAttrs = new PrimaryAttribute(1,1,8,5);
        assert testHero.getBaseAttrs().equals(testAttrs);
    }

    @Test
    void levelUp_testingLevelUpAttributes_expecting_2_2_13_8() {
        Hero testHero = new Hero("John");
        testHero.giveClass(new Mage());
        testHero.levelUp();
        PrimaryAttribute testAttrs = new PrimaryAttribute(2,2,13,8);
        assert testHero.getBaseAttrs().equals(testAttrs);
    }

}