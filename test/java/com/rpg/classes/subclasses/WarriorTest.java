package com.rpg.classes.subclasses;

import com.rpg.character.Hero;
import com.rpg.character.attributes.PrimaryAttribute;
import com.rpg.item.exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {
    @Test
    void warriorLevelOne_testingLevelOneAttributes_expecting_5_2_1_10() {
        Hero testHero = new Hero("John");
        testHero.giveClass(new Warrior());
        PrimaryAttribute testAttrs = new PrimaryAttribute(5,2,1,10);
        assert testHero.getBaseAttrs().equals(testAttrs);
    }

    @Test
    void warriorLevelUp_testingLevelUpAttributes_expecting_8_4_2_15() {
        Hero testHero = new Hero("John");
        testHero.giveClass(new Warrior());
        testHero.levelUp();
        PrimaryAttribute testAttrs = new PrimaryAttribute(8,4,2,15);
        assert testHero.getBaseAttrs().equals(testAttrs);
    }
}