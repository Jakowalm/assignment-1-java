package com.rpg.classes.subclasses;

import com.rpg.character.Hero;
import com.rpg.character.attributes.PrimaryAttribute;
import com.rpg.item.exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {

    @Test
    void rangerLevelOne_testingLevelOneAttributes_expecting_1_7_1_8() {
        Hero testHero = new Hero("John");
        testHero.giveClass(new Ranger());
        PrimaryAttribute testAttrs = new PrimaryAttribute(1,7,1,8);
        assert testHero.getBaseAttrs().equals(testAttrs);
    }

    @Test
    void rangerLevelUp_testingLevelUpAttributes_expecting_2_12_2_10() {
        Hero testHero = new Hero("John");
        testHero.giveClass(new Ranger());
        testHero.levelUp();
        PrimaryAttribute testAttrs = new PrimaryAttribute(2,12,2,10);
        assert testHero.getBaseAttrs().equals(testAttrs);
    }
}