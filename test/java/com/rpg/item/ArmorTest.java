package com.rpg.item;

import com.rpg.character.Hero;
import com.rpg.character.attributes.PrimaryAttribute;
import com.rpg.classes.subclasses.Warrior;
import com.rpg.item.exceptions.InvalidArmorException;
import com.rpg.item.exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ArmorTest {

    @Test
    void getAttributes_testingStatsForArmor_shouldReturn_1_0_0_2() {
        Armor testPlateBody = new Armor();
        testPlateBody.setName("Common Plate Body Armor");
        testPlateBody.setRequiredLevel(1);
        testPlateBody.setItemSlot(Item.slot.BODY);
        testPlateBody.setType(Item.types.PLATE);
        testPlateBody.setStats(new PrimaryAttribute(1,0,0,2));

        PrimaryAttribute testStats = new PrimaryAttribute(1,0,0,2);
        assert testPlateBody.getArmorStats().equals(testStats);
    }

    @Test
    void tryEquip_okArmor_shouldBeTrue() throws InvalidWeaponException, InvalidArmorException {
        Armor testPlateBody = new Armor();
        testPlateBody.setName("Common Plate Body Armor");
        testPlateBody.setRequiredLevel(1);
        testPlateBody.setItemSlot(Item.slot.BODY);
        testPlateBody.setType(Item.types.PLATE);
        testPlateBody.setStats(new PrimaryAttribute(1,0,0,2));

        Hero testHero = new Hero("John");
        testHero.giveClass(new Warrior());
        assert testHero.giveItem(testPlateBody);
    }

    @Test
    void getDPS_onlyArmor_shouldBe() throws InvalidWeaponException, InvalidArmorException {
        Armor testPlateBody = new Armor();
        testPlateBody.setName("Common Plate Body Armor");
        testPlateBody.setRequiredLevel(1);
        testPlateBody.setItemSlot(Item.slot.BODY);
        testPlateBody.setType(Item.types.PLATE);
        testPlateBody.setStats(new PrimaryAttribute(1,0,0,2));

        Hero testHero = new Hero("John");
        testHero.giveClass(new Warrior());
        testHero.giveItem(testPlateBody);
        assert testHero.getDPS() == 1*(1 + ((5+1) / 100.0));
    }

    @Test
    void tryEquip_tooHighLevel_shouldThrowError() {
        Armor testPlateBody = new Armor();
        testPlateBody.setName("Common Plate Body Armor");
        testPlateBody.setRequiredLevel(2);
        testPlateBody.setItemSlot(Item.slot.BODY);
        testPlateBody.setType(Item.types.PLATE);
        testPlateBody.setStats(new PrimaryAttribute(1,0,0,2));

        Hero testHero = new Hero("John");
        testHero.giveClass(new Warrior());

        Assertions.assertThrows(InvalidArmorException.class,() -> {
            testHero.giveItem(testPlateBody);
        });
    }

    @Test
    void tryEquip_wrongArmorType_shouldThrowError() {
        Armor testPlateBody = new Armor();
        testPlateBody.setName("Common Plate Body Armor");
        testPlateBody.setRequiredLevel(1);
        testPlateBody.setItemSlot(Item.slot.BODY);
        testPlateBody.setType(Item.types.CLOTH);
        testPlateBody.setStats(new PrimaryAttribute(1,0,0,2));

        Hero testHero = new Hero("John");
        testHero.giveClass(new Warrior());

        Assertions.assertThrows(InvalidArmorException.class,() -> {
            testHero.giveItem(testPlateBody);
        });
    }
}