package com.rpg.item;

import com.rpg.character.Hero;
import com.rpg.classes.subclasses.Warrior;
import com.rpg.item.exceptions.InvalidArmorException;
import com.rpg.item.exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class WeaponTest {

    @Test
    void getDps_testingDPSForWeapon_shouldReturn7point7() {
        Weapon testWeapon = new Weapon();
        testWeapon.setName("Common Axe");
        testWeapon.setRequiredLevel(1);
        testWeapon.setItemSlot(Item.slot.WEAPON);
        testWeapon.setType(Item.types.AXE);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);
        assert testWeapon.getDPS() == 7.7;
    }

    @Test
    void tryEquip_okWeapon_shouldBeTrue() throws InvalidWeaponException, InvalidArmorException {
        Weapon testWeapon = new Weapon();
        testWeapon.setName("Common Axe");
        testWeapon.setRequiredLevel(1);
        testWeapon.setItemSlot(Item.slot.WEAPON);
        testWeapon.setType(Item.types.AXE);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);

        Hero testHero = new Hero("John");
        testHero.giveClass(new Warrior());
        assert testHero.giveItem(testWeapon);
    }

    @Test
    void tryEquip_tooHighLevel_shouldThrowError() {
        Weapon testWeapon = new Weapon();
        testWeapon.setName("Common Axe");
        testWeapon.setRequiredLevel(2);
        testWeapon.setItemSlot(Item.slot.WEAPON);
        testWeapon.setType(Item.types.AXE);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);

        Hero testHero = new Hero("John");
        testHero.giveClass(new Warrior());

        Assertions.assertThrows(InvalidWeaponException.class,() -> {
            testHero.giveItem(testWeapon);
        });
    }

    @Test
    void tryEquip_wrongWeaponType_shouldThrowError() {
        Weapon testWeapon = new Weapon();
        testWeapon.setName("Wand");
        testWeapon.setRequiredLevel(1);
        testWeapon.setItemSlot(Item.slot.WEAPON);
        testWeapon.setType(Item.types.WAND);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);

        Hero testHero = new Hero("John");
        testHero.giveClass(new Warrior());

        Assertions.assertThrows(InvalidWeaponException.class,() -> {
            testHero.giveItem(testWeapon);
        });
    }

}