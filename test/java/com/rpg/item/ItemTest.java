package com.rpg.item;

import com.rpg.character.Hero;
import com.rpg.character.attributes.PrimaryAttribute;
import com.rpg.classes.subclasses.Warrior;
import com.rpg.item.exceptions.InvalidArmorException;
import com.rpg.item.exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

class ItemTest {

    @Test
    void tryDPS_armorAndWeapon_shouldBe8_162() throws InvalidWeaponException, InvalidArmorException {
        Armor testPlateBody = new Armor();
        testPlateBody.setName("Common Plate Body Armor");
        testPlateBody.setRequiredLevel(1);
        testPlateBody.setItemSlot(Item.slot.BODY);
        testPlateBody.setType(Item.types.PLATE);
        testPlateBody.setStats(new PrimaryAttribute(1,0,0,2));

        Weapon testWeapon = new Weapon();
        testWeapon.setName("Common Axe");
        testWeapon.setRequiredLevel(1);
        testWeapon.setItemSlot(Item.slot.WEAPON);
        testWeapon.setType(Item.types.AXE);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);

        Hero testHero = new Hero("John");
        testHero.giveClass(new Warrior());
        testHero.giveItem(testPlateBody);
        testHero.giveItem(testWeapon);

        assert testHero.getDPS() == (7 * 1.1) * (1 + ((5+1) / 100.0));
    }

}