package com.rpg.character;

import com.rpg.classes.subclasses.Mage;
import com.rpg.classes.subclasses.Ranger;
import com.rpg.classes.subclasses.Rogue;
import com.rpg.classes.subclasses.Warrior;
import com.rpg.item.exceptions.InvalidArmorException;
import com.rpg.item.exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

class HeroTest {

    @Test
    void getLevel_testingFreshCharacter_ShouldEqual1() {
        Hero testHero = new Hero("John");
        assert(testHero.getLevel()==1);
    }

    @Test
    void levelUp_testingLevelUp_levelShouldEqual2() {
        Hero testHero = new Hero("John");
        testHero.levelUp();
        assert(testHero.getLevel()==2);
    }

    @Test
    void mageGetDPS_testingDPS_DPSShouldBeEqualTo1_05() throws InvalidWeaponException, InvalidArmorException {
        Hero testHero = new Hero("John");
        testHero.giveClass(new Mage());
        assert(testHero.getDPS() == 1.08);
    }

    @Test
    void rangerGetDPS_testingDPS_DPSShouldBeEqualTo1_05() throws InvalidWeaponException, InvalidArmorException {
        Hero testHero = new Hero("John");
        testHero.giveClass(new Ranger());
        assert(testHero.getDPS() == 1.07);
    }

    @Test
    void rogueGetDPS_testingDPS_DPSShouldBeEqualTo1_05() throws InvalidWeaponException, InvalidArmorException {
        Hero testHero = new Hero("John");
        testHero.giveClass(new Rogue());
        assert(testHero.getDPS() == 1.06);
    }

    @Test
    void warriorGetDPS_testingDPS_DPSShouldBeEqualTo1_05() throws InvalidWeaponException, InvalidArmorException {
        Hero testHero = new Hero("John");
        testHero.giveClass(new Warrior());
        assert(testHero.getDPS() == 1.05);
    }
}